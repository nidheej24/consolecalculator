package consoleCalculator;

public interface IResultEvaluator {

	float evaluate(String expression) throws Exception;
}
