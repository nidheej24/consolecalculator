This is a console calculator Application.
Aim of the project is to get an expression from console and evaluate it based on the operation precedence.

Following are some features implemented:

1. Accepts Negative values.
2. "PI" is accepted and it is replaced by 3.1415927
3. "E" is accepted and it is replaced by 2.71828
4. Check If the expression is valid or not.
5. Displays result up to three decimal value.
6. Does not accept float value as input.
7. Application terminates only after giving �exit or EXIT�.

Following are some expression examples with expected results:

1. -1-2+ --> Invalid Expression
2. -1-2 --> results -3
3. 1/0 --> Cannot divide by 0
4. 10/3 --> 3.333
5. 10/2+E-3*2 --> 1.718
6. 10/2+PI-3*2 --> 2.142

Unit Testing is done for the application using JUnit.

